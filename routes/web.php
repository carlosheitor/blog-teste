<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CategoriaController@index');

//rotas de categoria
Route::get('/categorias', 'CategoriaController@index');
Route::get('/categorias/show/{id}', 'CategoriaController@show');
Route::get('/categorias/remove/{id}', 'CategoriaController@remove');
Route::get('/categorias/create/', 'CategoriaController@create');
Route::post('/categorias/add/', 'CategoriaController@add');
Route::get('/categorias/edit/{id}', 'CategoriaController@edit');
Route::post('/categorias/update/', 'CategoriaController@update');

//rotas de posts
Route::get('/posts', 'PostController@index');
Route::get('/posts/show/{id}', 'PostController@show');
Route::get('/posts/remove/{id}', 'PostController@remove');
Route::get('/posts/create/', 'PostController@create');
Route::post('/posts/add/', 'PostController@add');
Route::get('/posts/edit/{id}', 'PostController@edit');
Route::post('/posts/update/', 'PostController@update');

Route::get('/about', function () {
    return view('about');
});

Route::get('admin/search', 'SearchController@getSearch');