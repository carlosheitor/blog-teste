<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriaRequest;
use App\Categoria;
use Session;

use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function index()
 	{
 		$categorias = Categoria::all(); // busca todos os produtos na tabela categorias
 		return view('categoria.index')->with('categorias', $categorias);
 	}

 	public function show($id) {
 		$categoria = Categoria::find($id);

 		return view('categoria.show')->with('categoria', $categoria);
 	}

 	public function create() {
 		return view('categoria.create')->with('categorias', Categoria::all());
 	}

 	public function add(CategoriaRequest $request) { 		
 		Categoria::create($request->all()); //Criando uma nova categoria

 		Session::flash('categoria_new', 'Categoria Criada');

 		return redirect('/categorias')->withInput(); 
 	}

 	public function edit($id) {
 		$categoria = Categoria::find($id);
 
 		return view('categoria.edit')->with('categoria', $categoria);
 	}

 	public function remove($id) {
 		try {
	 		$categoria = Categoria::find($id);
	 		$categoria->delete();
	 		Session::flash('categoria_delete', 'Categoria Deletada');
	 		return redirect('/categorias');
 		} catch (QueryException $e) {
	 		Session::flash('categoria_error', 'Categoria erro');
        	return redirect()->back();
 		}
 	}

 	public function update(CategoriaRequest $request) { 	
 		$categoria = Categoria::find($request->id);
 		$categoria->nome = $request->Input('nome');
 		$categoria->save();

 		Session::flash('categoria_update', 'Categoria Atualizada');

    	return redirect("/categorias/show/$request->id")->withInput();
 	}

}
