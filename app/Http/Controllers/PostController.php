<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Categoria;
use App\Post;
use Session;

class PostController extends Controller
{
    public function index()
 	{
 		$posts = Post::all(); // busca todos os produtos na tabela posts
 		$categorias = Categoria::pluck('nome', 'id');
 		return view('post.index')->with('posts', $posts)->with('categorias', $categorias);
 	}

 	public function create() {
 		$categorias = Categoria::all();
 		return view('post.create')->with('categorias', $categorias);
 	}

 	public function show($id) {
 		$post = Post::find($id);

 		return view('post.show')->with('post', $post);
 	}

 	public function add(PostRequest $request) { 		
 		Post::create($request->all()); //Criando um novo post

 		Session::flash('post_new', 'Post Criado');

 		return redirect('/posts')->withInput(); 
 	}

 	public function edit($id) {
 		$post = Post::find($id);
 		$categorias = Categoria::all();
 
 		return view('post.edit')->with('post', $post)->with('categorias', $categorias);
 	}

 	public function remove($id) {
 		$post = Post::find($id);
 		$post->delete();
 		Session::flash('post_delete', 'Post Deletado');

 		return redirect('/posts');
 	}

 	public function update(PostRequest $request) { 	
 		$post = Post::find($request->id);
 		$post->titulo = $request->Input('titulo');
 		$post->categoria_id = $request->Input('categoria_id');
 		$post->autor = $request->Input('autor');
 		$post->descricao = $request->Input('descricao');
 		$post->texto = $request->Input('texto');
 		$post->save();

 		Session::flash('post_update', 'Post Atualizado');

    	return redirect("/posts/show/$request->id")->withInput();
 	}
}
