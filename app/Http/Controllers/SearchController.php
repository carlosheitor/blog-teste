<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Categoria;
use App\Post;
use Response;

class SearchController extends Controller
{
	public function getSearch(Request $request) {
		
		$keyword = $request->input('keyword');
		$categ =  $request->input('categoria');
		$categorias = Categoria::pluck('nome', 'id');

		if (($keyword != "") && ($categ != "")){
			return view('post.index')
			->with('posts', Post::where('titulo', 'LIKE', '%'.$keyword.'%')->where('categoria_id', '=',(int) $categ)->get())
			->with('keyword', $keyword)
			->with('categoria', $categ)
			->with('categorias', $categorias);
		} elseif (($keyword != "") && ($categ == "")) {
			return view('post.index')
			->with('posts', Post::where('titulo', 'LIKE', '%'.$keyword.'%')->get())
			->with('keyword', $keyword)
			->with('categorias', $categorias);
		} elseif (($keyword == "") && ($categ != "")) {
			return view('post.index')
			->with('posts', Post::where('titulo', 'LIKE', '%'.$keyword.'%')->where('categoria_id', '=',(int) $categ)->get())
			->with('keyword', $keyword)
			->with('categoria', $categ)
			->with('categorias', $categorias);
		} else {
			return redirect('/posts');
		}
	}
}
