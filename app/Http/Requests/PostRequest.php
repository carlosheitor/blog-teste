<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
 
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required|min:5|max:20',
            'autor' => 'required|min:3|max:20',
            'categoria_id' => 'required|integer',
            'descricao' => 'required|min:10|max:50',
            'texto' => 'required|min:20|max:100'
        ];
    }

    //customizar as mensagens. O nome da função precisa ser 'messages'
    public function messages() {
        return [
            'required'=>":attribute é obrigatório.",
            'min' => ":attribute precisa ter no mínimo :min caracteres.",
            'max' => ":attribute precisa ter no máximo :max caracteres." 
        ];
    }   
}
