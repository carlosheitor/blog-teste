<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
 
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|min:3'
        ];
    }

    //customizar as mensagens. O nome da função precisa ser 'messages'
    public function messages() {
        return [
            'required'=>"O :attribute é obrigatório.",
            'min' => "O nome precisa ter no mínimo três caracteres." 
        ];
    }
}
