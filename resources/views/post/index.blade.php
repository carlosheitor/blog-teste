@extends('admin.index')

@section('conteudo')
<h1>Listagem de Posts</h1>
@if (Session::has('post_new'))
	<div class="alert alert-success">
		Post criado com sucesso!
	</div>
@endif
@if (Session::has('post_update'))
	<div class="alert alert-success">
		Post atualizado com sucesso!
	</div>
@endif
@if (Session::has('post_delete'))
	<div class="alert alert-danger">
		Post deletado com sucesso!
	</div>
@endif                      

@if (count($posts))

<div class="row">
  <div class="col-lg-6">
    {!! Form::open(array('url'=>'admin/search', 'method'=>'get')) !!}
    <div class="form-group">
	    {!! Form::text('keyword', null, array('placeholder'=>'Search', 'class'=>'form-control')) !!}
	    {{ Form::select('categoria', $categorias, null, ['placeholder' => 'Escolha uma Categoria..', 'class'=>'form-control']) }}
    <span class="input-group-btn">
        {!! Form::submit('search', array('class'=>'btn btn-default')) !!}
    </span>
    
    {!! Form::close() !!}
    </div>
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->  

<table class="table table-striped table-bordered">
	<tr>
		<th>Título</th>
		<th>Categoria</th>
		<th>Autor</th>
		<th>Descrição</th>
		<th>Texto do Post</th>
		<th colspan="3">Ações</th>
	</tr>

	@foreach($posts as $post) 
	<tr> 
		<td>{{$post->titulo}}</td>
		<td>{{$post->categoria->nome}}</td>
		<td>{{$post->autor}}</td>
		<td>{{$post->descricao}}</td>
		<td>{{$post->texto}}</td>

		<td>
			<a href="/posts/show/<?= $post->id ?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			</a>
		</td>
		<td>
			<a href="/posts/edit/<?= $post->id ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
			</a>
		</td>
		
		<td>
			<a href="/posts/remove/<?= $post->id ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
			</a>
		</td>
	</tr>
	@endforeach

</table>
@endif
<a href="/posts/create">Adicionar Post</a>
@stop