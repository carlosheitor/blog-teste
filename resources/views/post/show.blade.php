@extends('admin.principal')

@section('conteudo')
<h1>Detalhes do Post</h1>
@if (old('nome'))
	<div class="alert alert-success">
		Post {{old('nome')}} atualizado com sucesso!
	</div>
@endif
<ul>
	<li>Título: {{$post->titulo}}</li>
	<li>Categoria: {{$post->categoria->nome}}</li>
	<li>Autor: {{$post->autor}}</li>
	<li>Descrição: {{$post->descricao}}</li>
	<li>Texto do Post: {{$post->texto}}</li>
</ul>
@stop
