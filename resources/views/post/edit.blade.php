@extends('admin.index')

@section('conteudo')
<div class="alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>

<h1>Editando Post</h1>

<form action="/posts/update" method="post" >

	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">

		<td><input class="form-control" type="hidden" name="id" value="{{$post->id}}"></td>
	</div>

	<div class="form-group">
		
		<td>Título:</td>
		<td><input class="form-control" type="text" name="titulo" value="{{$post->titulo}}"></td>
	</div>

	<div class="form-group">

		<td>Categoria:</td>
		<td>
						<select name="categoria_id" class="form-control">

							<?php foreach ($categorias as $categoria) : 
								$essaEhACategoria = $post['categoria_id'] == $categoria['id'];

								$selecao = $essaEhACategoria ? "selected='selected'" : "";
								?>
								<option value="<?=$categoria['id']?>" <?=$selecao?>>
									<?=$categoria['nome']?>
								</option>	
							<?php endforeach ?>
						</select>
					</td>
	</div>

	<div class="form-group">
		
		<td>Autor:</td>
		<td><input class="form-control" type="text" name="autor" value="{{$post->autor}}"></td>
	</div>

	<div class="form-group">
		
		<td>Descrição:</td>
		<td><textarea class="form-control" type="text" name="descricao">{{$post->descricao}}</textarea></td>
	</div>

	<div class="form-group">
		
		<td>Texto do Post:</td>
		<td><textarea class="form-control" type="text" name="texto">{{$post->texto}}</textarea></td>
	</div>

	<button class="btn btn-primary" type="submit">Atualizar</button>
</form>
@stop