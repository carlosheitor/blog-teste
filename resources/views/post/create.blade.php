@extends('admin.index')

@section('conteudo')
<div class="alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>

<h1>Novo Post</h1>

<form action="/posts/add" method="post">

	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">
		
		<td>Título:</td>
		<td><input class="form-control" type="text" name="titulo"></td>
	</div>

	<div class="form-group">

		<td>Categoria:</td>
		<td>
			<select name="categoria_id" class="form-control">
				@foreach($categorias as $categoria)
					<option value="{{$categoria->id}}">{{$categoria->nome}}</option>
				@endforeach
			</select>
		</td>
	</div>

	<div class="form-group">
		
		<td>Autor:</td>
		<td><input class="form-control" type="text" name="autor"></td>
	</div>

	<div class="form-group">
		
		<td>Descrição:</td>
		<td><textarea class="form-control" type="text" name="descricao"></textarea></td>
	</div>

	<div class="form-group">
		
		<td>Texto do Post:</td>
		<td><textarea class="form-control" type="text" name="texto"></textarea></td>
	</div>

	<button class="btn btn-primary" type="submit">Adicionar</button>
</form>
@stop