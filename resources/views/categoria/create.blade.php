@extends('admin.index')

@section('conteudo')
<div class="alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>

<h1>Nova Categoria</h1>

<form action="/categorias/add" method="post">

	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">
		
		<td>Nome:</td>
		<td><input class="form-control" type="text" name="nome"></td>
	</div>

	<button class="btn btn-primary" type="submit">Adicionar</button>
</form>
@stop