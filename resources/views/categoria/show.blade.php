@extends('admin.principal')

@section('conteudo')
<h1>Detalhes da Categoria</h1>
@if (old('nome'))
	<div class="alert alert-success">
		Categoria {{old('nome')}} atualizado com sucesso!
	</div>
@endif
<ul>
	<li>Nome: {{$categoria->nome}}</li>
</ul>
@stop
