@extends('admin.index')

@section('conteudo')
<h1>Listagem de Categorias</h1>;
@if (Session::has('categoria_new'))
	<div class="alert alert-success">
		Categoria criada com sucesso!
	</div>
@endif
@if (Session::has('categoria_error'))
	<div class="alert alert-danger">
		NÃO PODE PORRA!!!
	</div>
@endif
@if (Session::has('categoria_update'))
	<div class="alert alert-success">
		Categoria atualizada com sucesso!
	</div>
@endif
@if (Session::has('categoria_delete'))
	<div class="alert alert-danger">
		Categoria deletada com sucesso!
	</div>
@endif

<table class="table table-striped table-bordered">
	<tr>
		<th>Nome</th>
		<th colspan="3">Ações</th>
	</tr>

	@foreach($categorias as $categ) 
	<tr> 
		<td><?=$categ->nome?></td>

		<td>
			<a href="/categorias/show/<?= $categ->id ?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			</a>
		</td>
		<td>
			<a href="/categorias/edit/<?= $categ->id ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
			</a>
		</td>
		
		<td>
			<a href="/categorias/remove/<?= $categ->id ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
			</a>
		</td>
	</tr>
	@endforeach

</table>
<a href="/categorias/create">Adicionar Categoria</a>
@stop