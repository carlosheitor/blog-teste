@extends('admin.index')

@section('conteudo')
<div class="alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>

<h1>Editando Categoria</h1>

<form action="/categorias/update" method="post" >

	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">

		<td><input class="form-control" type="hidden" name="id" value="{{$categoria->id}}"></td>
	</div>

	<div class="form-group">
		
		<td>Nome:</td>
		<td><input class="form-control" type="text" name="nome" value="{{$categoria->nome}}"></td>
	</div>

	<button class="btn btn-primary" type="submit">Atualizar</button>
</form>
@stop