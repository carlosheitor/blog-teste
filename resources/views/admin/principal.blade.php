<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Teste DaGema Publicidade</title>

    <!-- Bootstrap Core CSS -->
    {{Html::style("../vendor/bootstrap/css/bootstrap.min.css")}}

    <!-- MetisMenu CSS -->
    {{Html::style("../vendor/metisMenu/metisMenu.min.css")}}

    <!-- Custom CSS -->
    {{Html::style("../dist/css/sb-admin-2.css")}}

    <!-- Custom Fonts -->
    {{Html::style("../vendor/font-awesome/css/font-awesome.min.css")}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/categorias">DaGema Publicidade</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" href="/about">Sobre</a>
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                        
                        <li class="active">
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Categoria<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="active" href="/categorias/create">Criar Categoria</a>
                                </li>
                                <li>
                                    <a href="/categorias">Listar categorias</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Post<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="active" href="/posts/create">Criar Post</a>
                                </li>
                                <li>
                                    <a href="/posts">Listar Posts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                @yield('conteudo')
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    {{Html::script("../vendor/jquery/jquery.min.js")}}

    <!-- Bootstrap Core JavaScript -->
    {{Html::script("../vendor/bootstrap/js/bootstrap.min.js")}}

    <!-- Metis Menu Plugin JavaScript -->
    {{Html::script("../vendor/metisMenu/metisMenu.min.js")}}

    <!-- Custom Theme JavaScript -->
    {{Html::script("../dist/js/sb-admin-2.js")}}

</body>

</html>
